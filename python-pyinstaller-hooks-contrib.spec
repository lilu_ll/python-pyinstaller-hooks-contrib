Name:           python-pyinstaller-hooks-contrib
Version:        2024.7
Release:        1
Summary:        Community maintained hooks for PyInstaller
License:        Apache-2.0 or GPL-2.0-only
URL:            https://pypi.org/project/pyinstaller-hooks-contrib
Source:         https://files.pythonhosted.org/packages/source/p/pyinstaller_hooks_contrib/pyinstaller_hooks_contrib-%{version}.tar.gz

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-psutil
BuildRequires:  python3-pytest
BuildRequires:  python3-pyinstaller

BuildArch:      noarch

%description
Community maintained hooks for PyInstaller

%package -n python3-pyinstaller-hooks-contrib
Summary:        %{Summary}

%description -n python3-pyinstaller-hooks-contrib
Community maintained hooks for PyInstaller

%prep
%autosetup -p1 -n pyinstaller_hooks_contrib-%{version}

%build
%py3_build

%install
%py3_install

%check
export PYTHONPATH=%{buildroot}%{python3_sitelib}
pytest

%files -n python3-pyinstaller-hooks-contrib
%doc README.md
%license LICENSE LICENSE.APL.txt LICENSE.GPL.txt
%{python3_sitelib}/_pyinstaller_hooks_contrib/*
%{python3_sitelib}/pyinstaller_hooks_contrib-%{version}-py%{python3_version}.egg-info

%changelog
* Thu Aug 22 2024 yaoxin <yao_xin001@hoperun.com> - 2024.7-1
- Update to 2024.7:
  * Add hook for dbus_fast in order to collect submodules that are imported from 
    cythonized extensions. (#600)
  * Add hook for gribapi package from eccodes dist, in order to collect bundled headers 
    and ensure that the eccodes shared library is collected from the build environment. (#744)
  * Add hook for patoolib to collect dynamically-imported modules from the patoolib.programs sub-package. (#748)
  * Extend the xarray hook to collect additional backend plugins that are registered via the 
    xarray.backends entry-point (e.g., cfgrib). (#744)
  * Add hook for schwifty. Requires schwifty >= 2024.5.1 due to issues with data search path 
    in earlier versions. (#742)

* Tue Mar 05 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 2024.2-1
- Update package to version 2024.2
  Update sklearn.neighbors hook to account for removed hidden import

* Thu Oct 26 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2023.10-1
- Update package to version 2023.10

* Tue Aug 01 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 2023.6-1
- Update package to version 2023.6

* Mon Jul 10 2023 chenzixuan<chenzixuan@kylinos.cn> - 2023.5-1
- Update package to version 2023.5

* Wed Jun 21 2023 chendexi <chendexi@kylinos.cn> - 2023.3-1
- Update package to version 2023.3

* Fri May 12 2023 yaoxin <yao_xin001@hoperun.com> - 2023.2-1
- Package init
